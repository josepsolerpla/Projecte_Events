-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: Project_Events
-- ------------------------------------------------------
-- Server version 5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events_info`
--

DROP TABLE IF EXISTS `events_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_info` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text,
  `propietario` text,
  `dirigido_a` text,
  `presupuesto` text,
  `fecha` date DEFAULT NULL,
  `country` text,
  `province` text,
  `poblac` text,
  `ticket_price` int(11) DEFAULT '0',
  `img_event` text,
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_info`
--

LOCK TABLES `events_info` WRITE;
/*!40000 ALTER TABLE `events_info` DISABLE KEYS */;
INSERT INTO `events_info` VALUES (1,'asd1','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png'),(2,'asd2','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png'),(3,'asd3','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(4,'asd4','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(5,'asd5','asdasd','+12','255','2018-03-21','AX','Pontevedra','',25,'/Project_Events/media/default-avatar.png'),(6,'asdasd','asdasd','+12','255','2018-03-21','AX','Madrid','',25,'/Project_Events/media/default-avatar.png'),(7,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(8,'asdasd','asdasd','+12','255','2018-03-21','AX','Andalucia','',25,'/Project_Events/media/default-avatar.png'),(9,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(10,'asdasd','asdasd','+12','255','2018-03-21','AX','Barcelona','',25,'/Project_Events/media/default-avatar.png'),(11,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(12,'Diania','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png'),(13,'Dasdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(14,'Dlkjlkj','asdasd','+12','255','2018-03-21','AX','Barcelona','',25,'/Project_Events/media/default-avatar.png'),(15,'lkjlkj','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png'),(16,'Madridtest','madrid','+12','69000â‚¬','2018-03-26','ES','Madrid','Madrid',25,'/Project_Events/media/890214794-test.jpeg'),(17,'Madridtest2','algu','+12','25000','2018-03-26','ES','Madrid','Abetos, Los (moralzarzal)',15,'/Project_Events/media/default-avatar.png');
/*!40000 ALTER TABLE `events_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_tickets`
--

DROP TABLE IF EXISTS `events_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_tickets` (
  `id_event` int(11) DEFAULT NULL,
  `code_ticket` varchar(255) NOT NULL,
  `id_client` int(11) DEFAULT NULL,
  PRIMARY KEY (`code_ticket`),
  KEY `id_event` (`id_event`),
  KEY `id_client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_tickets`
--

LOCK TABLES `events_tickets` WRITE;
/*!40000 ALTER TABLE `events_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_login`
--

DROP TABLE IF EXISTS `reg_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_login` (
  `id_user` int(11) NOT NULL,
  `date` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `val` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `fk_reg_login_1` FOREIGN KEY (`id_user`) REFERENCES `users_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_login`
--

LOCK TABLES `reg_login` WRITE;
/*!40000 ALTER TABLE `reg_login` DISABLE KEYS */;
INSERT INTO `reg_login` VALUES (1,'2018-05-03 12:20:33','994af3d1748bbc891a86d61d3954754d','true'),(2,'2018-05-02 21:16:29','9c05f4abe36a4ca07631054fd474d88f','true');
/*!40000 ALTER TABLE `reg_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_like`
--

DROP TABLE IF EXISTS `user_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_like` (
  `id_user` varchar(45) NOT NULL,
  `id_event` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_like`
--

LOCK TABLES `user_like` WRITE;
/*!40000 ALTER TABLE `user_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `surname` text,
  `email` text,
  `phone` text,
  `password` text,
  `birthday` date DEFAULT NULL,
  `user_type` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` VALUES (1,'admin','admin','admin@gmail.com','+34123456789','admin','1998-07-28','admin'),(2,'user','user','user@gmail.com','+34123456788','user','1998-07-28','user'),(3,'owner','owner','owner@gmail.com','+34987654321','owner','1998-07-28','owner'),(16,'Yuse','Soler','josepsolerpla@gmail.com','+34963258741','josep','2018-02-27','user');
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info_FB`
--

DROP TABLE IF EXISTS `users_info_FB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info_FB` (
  `id_user` varchar(255) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info_FB`
--

LOCK TABLES `users_info_FB` WRITE;
/*!40000 ALTER TABLE `users_info_FB` DISABLE KEYS */;
INSERT INTO `users_info_FB` VALUES ('159573670','Yuse','josep_soler_pla','','http://pbs.twimg.com/profile_images/483616614672510976/X5Eoqnsa_normal.png','twitter'),('2068372189856716','Josep','Soler','josepsolerpla@gmail.com','https://lookaside.facebook.com/platform/profilepic/?asid=2068372189856716&height=100&width=100&ext=1525603713&hash=AeSxshQ7XUxTD_7t','facebook');
/*!40000 ALTER TABLE `users_info_FB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'Project_Events'
--

--
-- Dumping routines for database 'Project_Events'
--
/*!50003 DROP PROCEDURE IF EXISTS `login_reg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `login_reg`(IN id_ int,IN tok_ varchar(255))
BEGIN
  IF EXISTS(SELECT * FROM reg_login WHERE id_user=id_) THEN
    UPDATE reg_login SET token=tok_,date=now() WHERE id_user=id_;
  ELSE 
    INSERT INTO reg_login VALUES(id_,tok_,now(),"true");
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
