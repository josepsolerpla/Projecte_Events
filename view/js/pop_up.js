function SendAlert(message = "test"){
	$("#alertsbar").empty();
	var alert_1 = 
			'<div class="sd alert1">'+
				'<h3>'+message+'</h3>'+
				'<span class="close_alert">&times;</span>'+
			'</div>';
	$("#alertsbar").append(alert_1);
}
function SendAlert_01(message = "test"){
	$("#alertsbar").empty();
	var alert_1 = 
			'<div class="sd alert2">'+
				'<h3>'+message+'</h3>'+
				'<span class="close_alert">&times;</span>'+
			'</div>';
	$("#alertsbar").append(alert_1);
}
$(document).ready(function () {
	if (read_cookie("alert")) {
		var alert = read_cookie("alert");
		SendAlert(alert.message);
		delete_cookie("alert");
	};
	$(document).on("click",'.close_alert', function (a) {
		var b = a.originalEvent.target.parentElement;
		b.style.display = "none";
	});	
});
