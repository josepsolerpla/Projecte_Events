var tp = "";
$(document).ready(function () {
	if (read_cookie("user") != null) {
		var data = {'id':read_cookie("user").username,"token":read_cookie("user").token};
		petition({mod:"login",fun:"validate_key","data":data}).then(function (res) {		
		    res = JSON.parse(res);
		    if (res.rows != 0) {
		    	tp = res.type;
			    load_menu(tp);
		    }else{
		    	delete_cookie("user");
		    	not_log();	    	
			}
		});
	}else{
		delete_cookie("user");		
    	not_log();
	};
});
function load_menu(type) {
	switch(type){
		case 'admin':
			admin_menu()
			break;
		case 'user':
			user_menu();
			break;
		case 'creator':
			creator_menu()
			break;
		default:
			not_log();
			break;
	}
}
function not_log() {
	$('.box1').empty();
	$('#menu-top').empty();
	if (tp == "") {
		$('#menu-top').append(no_menu);
		$('.box1').append(box1_content1);		
	};
}
function user_menu() {
	$('.box1').empty();
	$('.box2').empty();
	$('.box3').empty();
	$('#menu-top').empty();
	if (tp == "user") {
		$('#menu-top').append(menu_u);
		$('.box1').append(box1_content2);
	    document.getElementById("username").innerHTML = read_cookie("user").username;
	};
}
function admin_menu() {
	$('.box1').empty();
	$('.box2').empty();
	$('.box3').empty();
	$('#menu-top').empty();
	if (tp == "admin") {
		$('#menu-top').append(menu_a);
		$('.box1').append(box1_content2);
	    document.getElementById("username").innerHTML = read_cookie("user").username;
	};
}
function creator_menu() {
	$('.box1').empty();
	$('.box2').empty();
	$('.box3').empty();
	$('#menu-top').empty();
	if (tp == "creator") {
		$('#menu-top').append(menu_c);
		$('.box1').append(box1_content2);
	    document.getElementById("username").innerHTML = read_cookie("user").username;
	};
}