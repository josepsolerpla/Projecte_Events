$('document').ready(function() {
  /*
    LOGIN FUNCTIONS
  */
  if (read_cookie("user") != null && document.getElementById("username")) {
    document.getElementById("username").innerHTML = read_cookie("user").username;
  };
  $(document).on('click','#enterlogin',function () {
    if (validatelogin()) {
      var data = {'search_factor':{'name':document.getElementById('username').value}};
      petition({mod:"users",fun:"check_exist",data:data}).then(function (resp){
        console.log(resp);
        if (resp != 0) {
          var data = {'search_factor':{'name':document.getElementById('username').value,"password":document.getElementById('password').value,"logs":true}};
          petition({mod:"users",fun:"check_exist",data:data}).then(function (resp){
            if (resp != 0) {
              bake_cookie("user",{"username":document.getElementById("username").value,"token":resp});
              bake_cookie("alert",{"target":"alert1","message":"Succesufful loggin"});
              window.location.href = "/Project_Events/";
            }else{
              document.getElementById("e_password").innerHTML = "Wrong password";
              document.getElementById("enterlogin").style.color = ("red");
            }
          }); /*petition end*/
        }else {
          document.getElementById("enterlogin").style.color = ("red");
          document.getElementById("e_username").innerHTML = "Username not exist";
        };
      });/*petition end*/
    }else{
      document.getElementById("enterlogin").style.color = ("red");
    };
  });
  $(document).on('click','#logout', function () {
    bake_cookie("alert",{"target":"alert1","message":"Log out"});
    delete_cookie("user");
    window.location.href = "/Project_Events/";
  });
  $(document).on('click','.perfil', function () {
    window.location.href = "/Project_Events/users/perfil/";
  });  
});