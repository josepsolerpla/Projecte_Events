$('document').ready(function() {
  /*
    FIREBASE 
  */
  firebase.initializeApp(config);
  
  var providerF = new firebase.auth.FacebookAuthProvider();
  var providerT = new firebase.auth.TwitterAuthProvider();
  var authService = firebase.auth();

  $(document).on('click','#loginfacebook',function() {
      authService.signInWithPopup(providerF)
          .then(function(result) {
              var user = result.additionalUserInfo.profile;
              var data = {"method":"facebook","id":user.id,"name":user.first_name,"last_name":user.last_name,"email":user.email,"photo":user.picture.data.url};
              petition({mod:"users",fun:"insert_into_firebase",data}).then(function (res) {
                res = JSON.parse(res);
                bake_cookie("user",{"username":res.username,"token":res.token});
                bake_cookie("alert",{"target":"alert1","message":"Succesufful logged by Facebook"});
                window.location.href = "/Project_Events/";
              });
          })
          .catch(function(error) {
              var message = "error : " + error;
              SendAlert(message);
              console.log('Detectado un error:', error);
          });
  })

  $(document).on('click','#logintwitter',function() {
      authService.signInWithPopup(providerT)
          .then(function(result) {
              var user = result.additionalUserInfo.profile;
              var data = {"method":"twitter","id":user.id,"name":user.name,"last_name":user.screen_name,"email":"","photo":user.profile_image_url};
              petition({mod:"users",fun:"insert_into_firebase",data}).then(function (res) {
                res = JSON.parse(res);
                bake_cookie("user",{"username":res.username,"token":res.token});
                bake_cookie("alert",{"target":"alert1","message":"Succesufful logged by Twitter"});
                window.location.href = "/Project_Events/";
              });
          })
          .catch(function(error) {
              var message = "error : " + error;
              SendAlert(message);
              console.log('Detectado un error:', error);
          });
  })

});