$(document).ready(function () {
	$("#loginmenu").append(modal_content);
	/*
		LOAD IF COOKIE HAS TOKEN
	*/
	if (read_cookie("user") == null) {
	    not_log();
	}else{
		petition({mod:"login",fun:"validate_key",data:{'id':read_cookie("user").username,"token":read_cookie("user").token}}).then(function (res) {		
		    res = JSON.parse(res);
		    if (res.rows == 1) {
			    tp = "user";
		    	user_menu();
		    }else{
		    	console.log("1");
		    	delete_cookie("user");
			    not_log();		    	
			}
		});
	};
	/*
		MODAL FUNCTIONS
	*/
	$(document).on('click',"#loginbutton",function() {
	    document.getElementById('loginmenu').style.display = "block";
	})
	$(document).on('click',".close",function() {
	    document.getElementById('loginmenu').style.display = "none";
	})
	window.onclick = function(event) {
	    if (event.target == document.getElementById('loginmenu')) {
	        document.getElementById('loginmenu').style.display = "none";
	    }
	};
});