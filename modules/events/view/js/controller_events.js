$(document).ready(function () {
	$(document).on('click',"#createbutt", function () {
        create_event();
	});

    if (document.getElementById("list_event") != null) {
        load_event();
    }
    if (document.getElementById("create_event") != null) {
        $(document).on('keyup',"#name", function() {
            name = $(this).val();
            var word = {"nombre":name};
            check_exist(word).then(function(data) {
                var resp = JSON.parse(data);
                    if (resp === "false") {
                        document.getElementById('e_name').innerHTML = "This name is in use";
                        document.getElementById('name').style.color = ("red");
                        document.getElementById('createbutt').disabled = true;
                    }else{
                        document.getElementById('e_name').innerHTML = "";
                        document.getElementById('name').style.color = ("black");
                        document.getElementById('createbutt').disabled = false;                      
                    }
            });
        });

        $("#date").datepicker({
            dateFormat: 'yy/mm/dd',
            //dateFormat: 'mm-dd-yy',
            changeMonth: true, changeYear: true,
            minDate: -90, maxDate: "+1M"
        });

        $("#dropzone").dropzone({
            url: "../../events/upload",
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "An error has occurred on the server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
            init: function () {
                this.on("success", function (file, response) {
                    $("#progress").show();
                    $("#bar").width('100%');
                    $("#percent").html('100%');
                    $('.msg').text('').removeClass('msg_error');
                    $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                    console.log("Response: "+response);
                });
            },
            removedfile: function (file, serverFileName) {
                var name = file.name;
                $.ajax({
                    type: "POST",
                    url: "../../events/delete",
                    data: "filename=" + name,
                    success: function (data) {
                        $("#progress").hide();
                        $('.msg').text('').removeClass('msg_ok');
                        $('.msg').text('').removeClass('msg_error');
                        $("#e_avatar").html("");

                        var json = JSON.parse(data);
                        if (json.res === true) {
                            var element;
                            if ((element = file.previewElement) !== null) {
                                element.parentNode.removeChild(file.previewElement);
                            } else {
                                return false;
                            }
                        } else { 
                            var element2;
                            if ((element2 = file.previewElement) !== null) {
                                element2.parentNode.removeChild(file.previewElement);
                            } else {
                                return false;
                            }
                        }

                    }
                });
            }
        });
        //Load DropDowns
        load_countries_v1();
        
        $("#province").empty();
        $("#province").append('<option value="" selected="selected">Select province</option>');
        $("#province").prop('disabled', true);
        $("#poblac").empty();
        $("#poblac").append('<option value="" selected="selected">Select city</option>');
        $("#poblac").prop('disabled', true);

        $("#country").change(function() {
            var country = $(this).val();
            var province = $("#province");
            var city = $("#poblac");

            if(country !== 'ES'){
                 province.prop('disabled', true);
                 city.prop('disabled', true);
                 $("#province").empty();
                 $("#poblac").empty();
            }else{
                 province.prop('disabled', false);
                 city.prop('disabled', false);
                 load_provinces_v1();
            }
        });

        $("#province").change(function() {
            var prov = $(this).val().split(':')[0];
            if(prov > 0){
                load_cities_v1(prov);
            }else{
                $("#poblac").prop('disabled', false);
            }
        });
    }

});