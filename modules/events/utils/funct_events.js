//FUNCTIONS FOR VALIDATE
function v_number(value) {
	if (value.length > 0) {
        var regexp = /^[0-9]+(\.[0-9]+)?$/;
        return regexp.test(value);
    }
    return false;
}
function v_word_v1(name) {
	if (name.length > 3) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(name);
    }
    return false;
}
function v_word_v2(name) {
    if (name.text() != "Select country") {
        return true;
    }
    return false;
}
function v_expr(val,expr) {
	if (val.length > 3) {
        var regexp = expr;
        return regexp.test(val);
    }
    return false;
}
//FUNCTIONS
function validate_data() {
	var result = true;
	
    var name = document.getElementById('name').value;
    var owner = document.getElementById('owner').value;
    var value = document.getElementById('value').value.replace(/[^a-zA-Z 0-9.]+/g,'');
    var tick_val = document.getElementById('ticket_price').value.replace(/[^a-zA-Z 0-9.]+/g,'')
    var country = $( "#country option:selected" );

    var v_name = v_word_v1(name);
    var v_owner = v_word_v1(owner);
    var v_value = v_number(value);
    var v_tick = v_number(tick_val);
    var v_country = v_word_v2(country);

	pass = true;
        if (!v_name) {
            document.getElementById('e_name').innerHTML = "The name is invalid";
            pass = false;
        }else{
            document.getElementById('e_name').innerHTML = "";
        };
        if (!v_owner) {
            document.getElementById('e_owner').innerHTML = "The owner is invalid";
            pass = false;
        }else{
            document.getElementById('e_owner').innerHTML = "";
        };
        if (!v_value) {
            document.getElementById('e_value').innerHTML = "The value is invalid";
            pass = false;
        }else{
            document.getElementById('e_value').innerHTML = "";
        };
        if (!v_tick) {
            document.getElementById('e_ticket_price').innerHTML = "The ticket price is invalid";
            pass = false;
        }else{
            document.getElementById('e_ticket_price').innerHTML = "";
        };
        if (!v_country) {
            document.getElementById('e_country').innerHTML = "You must choose a country";
            pass = false;
        }else{
            document.getElementById('e_country').innerHTML = "";
        };

	return pass;
}

function create_event() {
	val = validate_data();
	if (val == true) {
        var name = document.getElementById("name").value;
        var owner = document.getElementById("owner").value;
        var recommended = document.getElementById("recommended").value;
        var value = document.getElementById("value").value;
        var ticket_price = document.getElementById("ticket_price").value;
        var date = document.getElementById("date").value;
        var country = document.getElementById("country").value;
        var province = document.getElementById("province").value.split(':')[1];
        var poblac = document.getElementById("poblac").value;

        var event = {"name": name,"owner":owner,"recommended":recommended,"value":value,
                    "ticket_price":ticket_price,"date":date,"country":country,"province":province,"poblac":poblac};
	   	$.ajax({
            url: "../../events/insert_json",
            type: 'POST',
            data : {
                "event": event,   
            },
            success: function(response) {
                var resp = JSON.parse(response); 
                if(resp.success){
                    console.log(resp);
                    window.location.href = resp.redirect;
                }
            },
        })
        .fail(function() {
           console.log("ERROR"); 
        });
	};
}
function check_exist(word) {
    const promise = new Promise(function (resolve, reject) {
        $.ajax({
            url: "../../events/check_exist",
            type: 'POST',
            data: {
              word
            },
            success: function(data) {
                resolve(data);
                return(data);
            },
        })
        .fail(function() {
           console.log("ERROR"); 
        });
      })
    return promise;
}
function load_event() {
    $.ajax({
        url: "../../events/load",
        type: 'POST',
        success: function(response) {
            var resp = JSON.parse(response); 
            if (resp.event) {
                console.log(resp);
                $("#name").val(resp.event.name);
                $("#owner").val(resp.event.owner);
                $("#recommended").val(resp.event.recommended);
                $("#value").val(resp.event.value);
                $("#ticket_price").val(resp.event.ticket_price);
                $("#date").val(resp.event.date);
                $("#country").val(resp.event.country);
                $("#province").val(resp.event.province);
                $("#poblac").val(resp.event.poblac);
            }else
                window.location.href = "../../events/create_event";  
        },
    })
    .fail(function() {
       console.log("ERROR"); 
    });
}
//LOAD DROPDOWNS
function load_countries_v2(cad) {
    $.getJSON( cad, function(data) {
      $("#country").empty();
      $("#country").append('<option value="" selected="selected">Select country</option>');

      $.each(data, function (i, valor) {
        $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
      });
    })
    .fail(function() {
        alert( "error load_countries" );
    });
}

function load_countries_v1() {
    $.get( "../../events/load_country",
        function( response ) {
            //console.log(response);
            if(response === 'error'){
                load_countries_v2("resources/ListOfCountryNamesByName.json");
            }else{
                load_countries_v2("../../events/load_country");
            }
    })
    .fail(function(response) {
        load_countries_v2("resources/ListOfCountryNamesByName.json");
    });
}

function load_provinces_v2() {
    $.get("https://localhost/Project_Events/resources/provinciasypoblaciones.xml", function (xml) {
        $("#province").empty();
        $("#province").append('<option value="" selected="selected">Select province</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#province").append("<option value='"+id+':'+name+"'>" + name + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_provinces" );
    });
}

function load_provinces_v1() { //provinciasypoblaciones.xml - xpath
    $.get( "../../events/load_provinces",
        function( response ) {
            load_provinces_v2();
            /*      NO DATABASE CREATED 
            $("#province").empty();
            $("#province").append('<option value="" selected="selected">Select province</option>');
            var json = JSON.parse(response);
            var provinces=json.provinces;
            if(provinces === 'error'){
                load_provinces_v2();
            }else{
                for (var i = 0; i < provinces.length; i++) {
                    $("#province").append("<option value='"+provinces[i].id+':'+provinces[i].nombre+"'>" + provinces[i].nombre + "</option>");
                }
            }*/
    })
    .fail(function(response) {
        load_provinces_v2();
    });
}
function load_cities_v2(prov) {
    $.get("https://localhost/Project_Events/resources/provinciasypoblaciones.xml", function (xml) {
        $("#poblac").empty();
        $("#poblac").append('<option value="" selected="selected">Select city</option>');

        $(xml).find('provincia[id=' + prov + ']').each(function(){
            $(this).find('localidad').each(function(){
                 $("#city").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    })
    .fail(function() {
        alert( "error load_cities" );
    });
}

function load_cities_v1(prov) { 
    $.ajax({
        url: "../../events/load_city",
        type: 'POST',
        data : {
            "idPoblac": prov,   
        },
        success: function(response) {
            var json = JSON.parse(response);
            var cities=json.cities;
            $("#poblac").empty();
            $("#poblac").append('<option value="" selected="selected">Select city</option>');

            if(cities === 'error'){
                load_cities_v2(prov);
            }else{
                for (var i = 0; i < cities.length; i++) {
                    $("#poblac").append("<option value='" + cities[i].poblacion + "'>" + cities[i].poblacion + "</option>");
                }
            }
        },
    })
    .fail(function() {
        load_cities_v2(prov);
    });
}