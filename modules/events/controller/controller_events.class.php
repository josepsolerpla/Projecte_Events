<?php

class controller_events {
    function __construct() {
        //include(UTILS_EVENTS . "utils.inc.php");
        require_once(UTILS."upload.inc.php");
        $_SESSION['module'] = "events";
    }

    function create_events() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");
    
      loadView('modules/events/view/', 'create_events.html');

      require_once(VIEW_PATH_INC . "footer.html");
    }
    function results_events() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");
    
      loadView('modules/events/view/', 'results_events.php');

      require_once(VIEW_PATH_INC . "footer.html");
    }
    function insert_json() {
      if (empty($_SESSION['result_prodpic'])){
		      $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => "/Project_Events/media/default-avatar.png");
		}
		$event_pic = $_SESSION['result_prodpic'];
		
		$arrArgument = $_POST['event'];
		array_push($arrArgument, $event_pic['data']);
		$arrValue = false;
      	$path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/events/model/model/';
     	$arrValue = loadModel($path_model, "event_model", "create_events", $arrArgument);
   	
     	if ($arrValue){
	        $message = "Event has been successfull registered";
	    }else{
	        $message = "Problem ocurred registering a event";
	    }

	    $_SESSION['event'] = $arrArgument;
	    $_SESSION['message'] = $message;
	    $callback="../../events/results_events";

	    $jsondata['success'] = true;
	    $jsondata['redirect'] = $callback;
	    echo json_encode($jsondata);
    }

    function load() {
    	$jsondata = array();
	    if (isset($_SESSION['event'])) {
	        $jsondata["event"] = $_SESSION['event'];
	    }
	    if (isset($_SESSION['message'])) {
	        $jsondata["message"] = $_SESSION['message'];
	    }
	    close_session();
	    echo json_encode($jsondata);
    }

    function check_exist() {
    	$arrArgument = $_POST['word'];
		
		$arrValue = false;
      	$path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/events/model/model/';
     	$arrValue = loadModel($path_model, "event_model", "check_exist", $arrArgument);
     	if ($arrValue == 0){
	        echo json_encode("true");
	    }else{
	        echo json_encode("false");
	    }
    }

	function upload() {
		$result_prodpic = upload_files();
  		$_SESSION['result_prodpic'] = $result_prodpic; //Save on Session the picture
	}

	function delete() {
		$_SESSION['result_prodpic'] = array();
	    $result = remove_files();
	    if($result === true){
	      echo json_encode(array("res" => true));
	    }else{
	      echo json_encode(array("res" => false));
	    }
	}

	function load_country() {
		$json = array();
    	$url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
		$path_model='/var/www/html/Project_Events/modules/events/model/model/';
		$json = loadModel($path_model, "event_model", "obtain_countries", $url);
		
		if($json){
			echo $json;
			exit;
		}else{
			$json = "error";
			echo $json;
		}
	}

	function load_city() {
		$jsondata = array();
        $json = array();

		$path_model='/opt/lampp/htdocs/Project_Events/modules/events/model/model/';
		$json = loadModel($path_model, "event_model", "obtain_cities", $_POST['idPoblac']);

		if($json){
			$jsondata["cities"] = $json;
			echo json_encode($jsondata);
		}else{
			$jsondata["cities"] = "error";
			echo json_encode($jsondata);
		}
	}
	function load_provinces() {
		echo json_encode("error");
	}
}
function close_session() {
	    unset($_SESSION['event']);
	    unset($_SESSION['message']);
	    $_SESSION = array();
	    session_destroy();
	}