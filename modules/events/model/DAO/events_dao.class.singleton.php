<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class events_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_events_DAO($db, $arrArgument) {
        $nombre = $arrArgument['name'];
        $propietario = $arrArgument['owner'];
        $dirigido_a = $arrArgument['recommended'];
        $presupuesto = $arrArgument['value'];
        $fecha = $arrArgument['date'];
        $country = $arrArgument['country'];
        $province = $arrArgument['province'];
        $poblac = $arrArgument['poblac'];
        $ticket_price = $arrArgument['ticket_price'];
        $img_event = $arrArgument['0'];

        
        $sql = "INSERT INTO events_info (nombre, propietario, dirigido_a, presupuesto,fecha, country, province, poblac, ticket_price, img_event) "
                . " VALUES ('$nombre', '$propietario','$dirigido_a', '$presupuesto', '$fecha', '$country','$province', '$poblac', '$ticket_price', '$img_event')";
        return $db->execute($sql);
    }

    public function check_exist_DAO($db, $arrArgument,$type = "exist" ) {
      $sql = "SELECT * FROM events_info ";
      $e = 0;
      foreach($arrArgument as $key => $val){
        if ($e == 0) {
          $sql = $sql . "WHERE $key LIKE '$val%' ";
          $e++;
        }else{
          $sql = $sql . "AND $key LIKE '$val%' ";
        }
      }

      if ($type == "exist") {
        $sql = "SELECT nombre FROM events_info WHERE nombre LIKE '$arrArgument[nombre]'";
      }
      $rows = $db->execute($sql)->num_rows; 
      return $rows;
    }

    public function search_events_keyword_DAO($db, $arrArgument) {
      $sql = "SELECT * FROM events_info ";
      $e = 0;
      
      foreach($arrArgument['search_factor'] as $key => $val){
        if ($e == 0) {
          $sql = $sql . "WHERE $key LIKE '$val%' ";
          $e++;
        }else{
          $sql = $sql . "AND $key LIKE '$val%' ";
        }
      }
      if (array_key_exists('page', $arrArgument)) {
        $sql = $sql . "LIMIT $arrArgument[page],9";
      }
      //echo json_encode($sql);
      //die;
      return $db->list_db($db,$sql);
    }

    public function mostview_events_DAO($db, $arrArgument) {
      $limit = $arrArgument["limit"];
      $type = $arrArgument["type"];

      $sql = "SELECT $type,count($type) FROM events_info WHERE $type NOT LIKE '' or NOT NULL GROUP BY $type ORDER BY count($type) DESC LIMIT 0,$limit";

      return $db->list_db($db,$sql);
    }

    public function obtain_countries_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }

    public function obtain_provinces_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Project_Events/resources/provinciasypoblaciones.xml');
          $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }

    public function obtain_cities_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Project_Events/resources/provinciasypoblaciones.xml');
          $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }
}
