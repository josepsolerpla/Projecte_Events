function petition({mod,fun,data = {}}) {                                                    /* PETITION */
    const promise = new Promise(function (resolve, reject) {
        $.ajax({
            url: "../../"+mod+"/"+fun,
            type: 'POST',
            data : {
                "data": data,   
            },
            success: function(response) { 
                resolve(response);
                return response;
            },
        })
        .fail(function() {
            reject();
           return "error"; 
        });
    })
    return promise;
}
function printevent_div(events) {                                                           /* PRINT DIVS*/
    events.forEach(function(rdo){
        var newElement = document.createElement('div');
        newElement.id = "event_div";
        newElement.className = rdo.id_event;
        newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.fecha+'</p>\<a id="viewonmap">ViewInMap</a>';
        document.getElementById("result_search").appendChild(newElement);
    });
}
function delete_container(){                                                                /* DELETE CONTAINERS*/
    if (document.getElementById("error_notexist")) {document.getElementById("error_notexist").remove();};
    while(document.getElementById("event_div")){
        document.getElementById("event_div").remove();
    };
}
function viewonmap(){
    $(document).on('click','#viewonmap', function (asd) {
        var id = asd.target.parentElement.className;
        var data = {'search_factor':{'id_event':id}};
        petition({mod:"homepage",fun:"get_event",data:data}).then(function (resp){
            var event = JSON.parse(resp);
            var data = {"place":event[0].province};
            petition({mod:"homepage",fun:"get_location",data}).then(function (response) {
                var location = JSON.parse(response).results;
                var langlat = location[0].geometry.location;
                newlocation(langlat.lat,langlat.lng);
            });  
        });
    });
}
function switchScrollNormal() {
    $("input[type=checkbox]").switchButton({                                                /*JQUARY SWITCH BUTTON*/
          on_label: 'Normal View',
          off_label: 'Scroll View'
        });
    $("input[type=checkbox]").change(function() {
        if (this.checked) {     //NORMAL VIEW
            return "normal";
        } else {                //SCROLL VIEW
            return "scroll";
        }
    });
    return "normal";
}
function load_bootpage() {                                                                  /* LOAD BOOTPAGE */
    var newElement = document.createElement('div');
    newElement.id = "bootpage";
    newElement.className = "bootpage";
    document.getElementById("homepage_content").appendChild(newElement);
}
function get_cityselected(type) {
    var cityselect = document.getElementsByName(type);
    for(var i = 0; i < cityselect.length; i++) {
       if(cityselect[i].checked){
        selectedCity = cityselect[i].value;
        return selectedCity;
       }    
     }
     return ""
}
/*
    CONTROLLER FUNCTIONS
*/
function keyword_pages(page,search_factor) {                                               /* SEARCH KEYWORD FOR BOOTPAGE */
    $("#bootpage").unbind("page"); //IMPORTANT , unbind a event
    var data = {"search_factor":search_factor};
    petition({mod:"homepage",fun:"count_eventdata",data : data}).then(function (cont) {
        var totalpage = Math.trunc(parseInt(cont)/9)+1;	//Search for the total number of events to create pages
        if (cont == 9) { totalpage = 1}
        
        search_factor['nombre'] = search_factor['nombre'].replace("%", "");
        var data = {"page":page,"search_factor":search_factor};
        petition({mod : "homepage", fun : "load_eventdata" , data : data}).then(function (resp) {
            var json = JSON.parse(resp);
            //console.log(json);
	    	if (json == "not exist") {
		    	$("#result_search")[0].innerHTML = '<h1 id="error_notexist">'+json+'</h1>';	    		
	    	}else{
                //console.log("entra 1");
                delete_container();
	    		printevent_div(json);
	    	}
	    });

		$("#bootpage").bootpag({
	            total: totalpage,
	            page: 1,
	            maxVisible: 3,
                disabledClass: 'active'
	        }).on("page", function (e, num) {
                page=num;
                //console.log(search_factor);
                var data = {"page":page,"search_factor":search_factor};
                petition({mod : "homepage", fun : "load_eventdata" , data : data}).then(function (resp) {
	            	var json = JSON.parse(resp);
	            	if (json == "not exist") {
				    	$("#result_search")[0].innerHTML = '<h1 id="error_notexist">'+json+'</h1>';	    		
			    	}else{
                        delete_container();
			    		printevent_div(json);
			    	}
	            });
	        });
	});
}
function keyword_scroll(page,search_factor){                                                /* SEARCH KEWORD FOR SCROLL*/
    //console.log("keyword_scroll page : "+page);
    search_factor['nombre'] = search_factor['nombre'].replace("%", "");
    var data = {"page":page,"search_factor":search_factor};
    petition({mod : "homepage", fun : "load_eventdata" , data : data}).then(function (resp) {
        //console.log(resp);
        var json = JSON.parse(resp);
        //console.log(json);
        if (json == "not exist") {
            $("#result_search")[0].innerHTML = '<h1 id="error_notexist">'+json+'</h1>';             
        }else{
            printevent_div(json);
        }
    });
}

function load_modal(search_factor) {                                                        /* LOAD MODAL */
    var data = {"search_factor":search_factor};
    petition({mod:"homepage",fun:"get_event",data}).then(function (resp) {
        var event = JSON.parse(resp);
        $("#img_event").html('<img src="' + event[0].img_event + '" height="75" width="75"> ');
        $("#name_event").html(event[0].nombre);
        $("#fecha_event").html(event[0].fecha);

        $("#details_event").show();

        $("#event").dialog({
            width: 850, 
            height: 500,
            resizable: "false", 
            modal: "true",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            },
        });
    });
}

function load_category(type,limit) {
    while(document.getElementById("category")){
        document.getElementById("category").remove();
    };
    var data = {"type":type,"limit":limit};
    petition({mod:"homepage",fun:"mostview_events",data:data}).then(function (resp){
        var json = JSON.parse(resp);
        json.forEach(function (a){
            var label = document.createElement('label');
            label.id = "category";
            label.innerHTML = '<input type="radio" name="cityselectpage" id="cityselectpage" value="'+a.province+'"/>';
            var url = "/Project_Events/media/"+a.province+".jpg";
            $.get(url)
            .done(function() { 
                label.innerHTML = label.innerHTML + '<img id="imagechoose" src="/Project_Events/media/'+a.province+'.jpg">\<br/>';
            }).fail(function() { 
                label.innerHTML = label.innerHTML + '<img id="imagechoose" src="/Project_Events/media/default-city.jpg">\<br/>';
                label.innerHTML = label.innerHTML + '<p id="cityname_choose">'+a.province.toUpperCase()+'</p>';
            })
            document.getElementById("city_selector").appendChild(label);
        })
    });
}

function load_autocomplete(search_factor) {                                                  /* LOAD AUTOCOMPLETE */
    if (search_factor == null) { var search_factor={"nombre":""}}
    search_factor['nombre'] = '%'+search_factor['nombre'];
    var data = {"search_factor":search_factor};
    petition({mod:"homepage",fun:"load_eventdata",data:data}).then(function (resp) {
        var source_names = [];
        var events = JSON.parse(resp);
        if (events == "not exist") {
            source_names = "not exist"
        }else {
            events.forEach(function(row) {
                source_names.push(row.nombre);            
            });
        }
        $("#keyword_normalview").autocomplete({
                source: source_names,
            });
    });
}

/*
    VIEWS
*/

function normal_view() {                                                                    /* NORMAL VIEW */
    $(document).on('click',"#tester", function () {
        
    });

    $(".input_keyword")[0].id = "keyword_normalview";
    $(".input_keyword")[0].value = "";
    load_bootpage();
    delete_container();

    /*
    EVENTS

    GET KEYWORD
    */
    $(document).off();
    $(document).on('input','#keyword_normalview', function () {
        keyword = $(this).val();
        var selectedCity = get_cityselected("cityselectpage");
        page = 1;
        search_factor = {"nombre":keyword,"province":selectedCity};
        keyword_pages(page,search_factor);
        load_autocomplete(search_factor);
    });
    
    /*
    PRELOAD DATA AND BOOTPAG
    */

    var page = 1;
    var keyword = $("#keyword_normalview").val();
    var selectedCity = get_cityselected("cityselectscroll");
    var search_factor = {"nombre":keyword,"province":selectedCity};
    keyword_pages(page,search_factor);


    /*
    LOAD MODAL
    */

    $(document).on('click','#event_div',function() {                                // ONLY LOAD ONCE
        var id_event = $(this).context.className
        var search_factor = {"id_event":id_event};                             // BECOUSE FIRST VIEW LOADED IS NORMAL
        load_modal(search_factor);
    });

    /*
    AUTOCOMPLETE
    */
    
    load_autocomplete();                                                            // ONLY LOAD ONCE
    
    /*
    RADDIO BUTTON
    */
    if (document.getElementsByName("cityselectscroll").length != 0) {
        for(x = 0; x < 3; x++){
            document.getElementsByName("cityselectscroll")[0].id = "cityselectpage";
            document.getElementsByName("cityselectscroll")[0].name = "cityselectpage";
        }
    }
    $("#cityselectpage").off();
    $("#cityselectscroll").off();
    $(document).on('click','#cityselectpage', function (re) {
        if (selectedCity == re.target.value) {
            this.checked = false;
            selectedCity = null;  
            keyword = $("#keyword_normalview").val();
            search_factor = {"nombre":keyword,"province":selectedCity};
            keyword_pages(1,search_factor);
        }else{
            selectedCity = re.target.value; 
            keyword = $("#keyword_normalview").val();
            search_factor = {"nombre":keyword,"province":selectedCity};
            keyword_pages(1,search_factor);           
        }
    });
    viewonmap();
}
function scroll_view() {                                                                    /* SCROLL VIEW */
    $(".input_keyword")[0].id = "keyword_scrollview";
    
    $(".input_keyword")[0].value = "";


    delete_container();
    document.getElementById("bootpage").remove();
    
    /*
    GET KEYWORD
    */
    
    var page = 0;
    $(document).off();
    $(document).on('input','#keyword_scrollview', function () {
        page = 1;
        keyword = $(this).val();
        var selectedCity = get_cityselected("cityselectscroll");
        search_factor = {"nombre":keyword,"province":selectedCity};
        delete_container();
        keyword_scroll(page,search_factor);
    });
    

    /*
        MODAL
    */
    $(document).on('click','#event_div',function() {                                // ONLY LOAD ONCE
        var id_event = $(this).context.className
        var search_factor = {"id_event":id_event};                             // BECOUSE FIRST VIEW LOADED IS NORMAL
        load_modal(search_factor);
    });
    /*
    PRELOAD DATA
    
    */
    page++;
    var keyword = $("#keyword_scrollview").val();
    var selectedCity = get_cityselected("cityselectpage");
    var search_factor = {"nombre":keyword,"province":selectedCity};
    keyword_scroll(page,search_factor);
    
    /*
    SCROLL DOWN FOR LOAD
    */
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       var data = {"search_factor":search_factor};
       petition({mod:"homepage",fun:"count_eventdata",data}).then(function (cont) {     
            var oldscroll = 0;
            var total_pages = Math.trunc(parseInt(cont)/9)+1;   //Search for the total number of events to create pages
            if (cont == 9) { total_pages = 1}
            if( st > oldscroll ){ //if we are scrolling down
                if( (st + $(window).height() >= $(document).height()  ) && (page < total_pages) ) {
                    page++;
                    keyword_scroll(page,search_factor);
                }
            }
        });
    });
    /*
    RADDIO BUTTON
    */
    if (document.getElementsByName("cityselectpage").length != 0) {
        for(x = 0; x < 3; x++){
            document.getElementsByName("cityselectpage")[0].id = "cityselectscroll";
            document.getElementsByName("cityselectpage")[0].name = "cityselectscroll";
        }
    }
    $("#cityselectscroll").unbind("click");
    $("#cityselectpage").unbind("click");
    var x = 1;
    $(document).on('click','#cityselectscroll', function (re) {
        x++;
        delete_container();
        if (selectedCity == re.target.value) {
            this.checked = false;
            selectedCity = null; 
            page = 1; 
            keyword = $("#keyword_scrollview").val();
            search_factor = {"nombre":keyword,"province":selectedCity};
            keyword_scroll(page,search_factor);
        }else{
            selectedCity = re.target.value; 
            keyword = $("#keyword_scrollview").val();
            page = 1;
            search_factor = {"nombre":keyword,"province":selectedCity};
            keyword_scroll(page,search_factor);           
        }
    });
    viewonmap();
}