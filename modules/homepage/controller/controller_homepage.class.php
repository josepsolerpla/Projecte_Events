<?php
class controller_homepage {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "homepage";
    }

    function load_homepage() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");

      loadView('modules/homepage/view/', 'homepage.html');

      require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_eventdata() {

      if (!isset($_POST['data']['page'])) {
  			$page = 1;
  		}else{
        $page = $_POST['data']['page'];
      }
  		if (!isset($_POST['data']['search_factor'])) {
  			$search_factor = array('nombre' => "");
  		}else {
        $search_factor = $_POST['data']['search_factor'];        
      }

  		$start_point = ($page - 1) * 9;

  		$arrArgument = array('page' => $start_point,'search_factor' => $search_factor);

  		$arrValue = false;
    	$path_model = MODEL_EVENTS;
     	$arrValue = loadModel($path_model, "event_model", "search_events_keyword", $arrArgument);
  		if ($arrArgument == "not exist") {
  			echo json_encode("no more");
  		}else
  			echo json_encode($arrValue);
    }

    function mostview_events() {
      $type = $_POST['data']['type'];
      $limit = $_POST['data']['limit'];

      $arrArgument = array('type' => $type, 'limit' => $limit);

      $arrValue = false;
      $path_model = MODEL_EVENTS;
      $arrValue = loadModel($path_model, "event_model", "mostview_events", $arrArgument);
      if ($arrArgument == "not exist") {
        echo json_encode("error");
      }else
        echo json_encode($arrValue);
    }

    function count_eventdata() {
    	
      $arrArgument = $_POST['data']['search_factor'];
  		
  		$arrValue = false;
      $path_model = MODEL_EVENTS;
      $arrValue = loadModel($path_model, "event_model", "count_eventdata", $arrArgument);
      
      echo json_encode($arrValue);
    }

    function get_event() {
    	$arrArgument = array('search_factor' => $_POST['data']['search_factor']);

  		$arrValue = false;
    	$path_model = MODEL_EVENTS;
     	$arrValue = loadModel($path_model, "event_model", "search_events_keyword", $arrArgument);
       	if ($arrArgument == "not exist") {
  			echo json_encode("no more");
  		}else
  			echo json_encode($arrValue);
    }
    function get_location() {
      $place = $_POST['data']['place'];
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=".$place."&key=AIzaSyBpHhlZbF2vhXzFC0XRhjsmQOu5YE4YlDk",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);
      echo($response);     
    } 
}
