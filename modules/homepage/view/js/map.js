var map;
function initMap() {
//Marcadors		lng lat
        var centre_iesestacio = {lat: 38.810145, lng: -0.604237};
//Centrar Mapa y Crear
        map = new google.maps.Map(document.getElementById('map_home'), {
          zoom: 6,
          center: centre_iesestacio
        });
// Movement
        //map.setCenter({lat:15,lng:15});
// Try HTML5 geolocation.
        var infoWindow = new google.maps.InfoWindow({map: map});
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('You are here.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
//Markers
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                      'Error: The Geolocation service failed.' :
                      'Error: Your browser doesn\'t support geolocation.');
}

function newlocation(lat,lng){
  map.setCenter({
    lat : lat,
    lng : lng
  });
  map.setZoom(10);
}