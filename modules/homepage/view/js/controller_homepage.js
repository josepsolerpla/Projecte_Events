$(document).ready(function () {
	
	var view = "normal";
	
	var selectedCity = "";																			// Value for country
    var keyword = "";																				// Value for seach bar

    var search_factor = {"nombre":keyword,"province":selectedCity};								// Array to send to jquary

    load_category('province',3);   
    
	$("input[type=checkbox]").switchButton({                                                    /*JQUARY SWITCH BUTTON*/
          on_label: 'Normal View',
          off_label: 'Scroll View'
        });
    $("input[type=checkbox]").change(function() {
        if (this.checked) {     //NORMAL VIEW
        	view = "normal";
        } else {                //SCROLL VIEW
        	view = "scroll";
        }
        switch(view) {
		    default:
			case "normal":
				normal_view();
		        break;
		    case "scroll":
		    	scroll_view();
		        break;
		}
    });

	normal_view();
});