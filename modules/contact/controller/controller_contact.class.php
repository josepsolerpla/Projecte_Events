<?php
class controller_contact {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "contact";
    }

    function load_contact() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");

      loadView('modules/contact/view/', 'contact.html');

      require_once(VIEW_PATH_INC . "footer.html");
    }
    function send_mail(){
      $email = $_POST['data'];

      $config = array();
      $config['api_key'] = "key-a22c9ffa2b5a26f149dfb6d0f6dd4cf6"; //API Key
      $config['api_url'] = "https://api.mailgun.net/v3/sandboxa13a728fefd7492ebf4c94a526f3ff14.mailgun.org/messages"; //API Base URL

      $message = array();
      $message['from'] = "josepsolerpla@gmail.com";
      $message['to'] = $email['to'];
      $message['h:Reply-To'] = "josepsolerpla@gmail.com";
      $message['subject'] = "Hello, this is a contact mail from Events Website";
      $message['html'] = "Hello, your contact subject with : ".$email['subject']." and message : ".$email['message']." has been sended correctly";
     
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $config['api_url']);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_POST, true); 
      curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
      $result = curl_exec($ch);
      curl_close($ch);

      echo json_encode($result);
    }
}