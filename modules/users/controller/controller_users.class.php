<?php
class controller_users {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "users";
    }

    function perfil() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");

      loadView('modules/users/view/', 'users.html');

      require_once(VIEW_PATH_INC . "footer.html");
    }

    function insert_into() {
      $arrArgument = $_POST['data'];
      $arrValue = false;
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/users/model/model/';
      $arrValue = loadModel($path_model, "users_model", "create_users", $arrArgument);

      echo json_encode($arrValue);
    }
    function insert_into_firebase() {
      $arrArgument = $_POST['data'];
      $arrValue = false;
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/users/model/model/';
      $arrValue = loadModel($path_model, "users_model", "create_users_firebase", $arrArgument);
      if ($arrValue['exists']) {
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $user = $arrValue['item'][0];
        $arrArgument = array("token" => $token , "id" => $user['id_user']);
        $arrValue = loadModel($path_model, "users_model", "log_login", $arrArgument);
        $back = array("token"=>$token,"username"=>$user['name']);
        echo json_encode($back);
      }else{
        echo "error";
      }
    }
    function read_user() {
      $arrArgument = $_POST['data'];
      $arrValue = false;
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/users/model/model/';
      
      $arrValue = loadModel($path_model, "users_model", "read_user", $arrArgument);
      if ($arrValue['exists'] == true) {
        $back = array("op"=>"enter","user"=>$arrValue['user']);
        echo json_encode($back);
      }else {
        echo array("op"=>"error","type"=>"not exists");
      }
    }
    function check_exist() {
      $arrArgument = $_POST['data']['search_factor'];
        if ($arrArgument['logs']) {
         $logs = $arrArgument['logs'];
         unset($arrArgument['logs']);
        }
      $arrValue = false;
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events/modules/users/model/model/';
      $arrValue = loadModel($path_model, "users_model", "check_exist", $arrArgument);
      if ($arrValue == 1) {
        if ($logs == "true") {
          $token = bin2hex(openssl_random_pseudo_bytes(16));
          $arrArgument = array("token" => $token , "name" => $_POST['data']['search_factor']['name']);
          $arrValue = loadModel($path_model, "users_model", "log_login", $arrArgument);
          echo ($token);
        }else
          echo "1";
      }else {
        echo("0");
      }
    }
}
