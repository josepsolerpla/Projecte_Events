<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class users_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_users_DAO($db, $arrArgument) {
        $name = $arrArgument['name'];
        $surname = $arrArgument['surname'];
        $email = $arrArgument['email'];
        $phone = $arrArgument['phone'];
        $password = $arrArgument['password'];
        $birthday = $arrArgument['birthday'];
        $user_type = "user";
        
        $sql = "INSERT INTO users_info VALUES ('$name', '$surname','$email', '$phone', '$password', '$birthday','$user_type')";
        return $db->execute($sql);
    }

    public function create_users_firebase_DAO($db, $arrArgument) {
        $id = $arrArgument['id'];
        $name = $arrArgument['name'];
        $surname = $arrArgument['last_name'];
        $email = $arrArgument['email'];
        $photo = $arrArgument['photo'];
        $method = $arrArgument['method'];
        
        $sql = "SELECT * FROM users_info_FB WHERE id_user = '$id'";
        $item = $db->list_db($db,$sql);
        $exits = $db->execute($sql)->num_rows;
        if ($exits == 0) {
          $sql = "INSERT INTO users_info_FB VALUES ('$id', '$name', '$surname','$email', '$photo', '$method', 'user')";
          $value = $db->execute($sql);   
        }else {
          $value = "exists";
        } 
        $back = array("exists" => $value , "item" => $item);
        return($back);       
    }

    public function check_exist_DAO($db, $arrArgument,$type = "exist" ) {
      $sql = "SELECT id FROM users_info ";
      $e = 0;
      foreach($arrArgument as $key => $val){
        if ($e == 0) {
          $sql .= "WHERE $key LIKE '$val' ";
          $e++;
        }else{
          $sql .= "AND $key LIKE '$val' ";
        }
      }
      if ($type == "exist") {
        $sql = "SELECT name FROM users_info WHERE name LIKE '$arrArgument[name]' UNION SELECT name FROM users_info_FB WHERE name LIKE '$arrArgument[name]'";
      }
      $rows = $db->execute($sql)->num_rows;
      return $rows;
    }
    public function log_login_DAO($db, $arrArgument) {
      if ($arrArgument['id']) {
        $sql = "CALL login_reg($arrArgument[id],'$arrArgument[token]');";
        return $db->execute($sql);
      }
      $sql = "SELECT id FROM users_info WHERE name = '$arrArgument[name]'";
      $id = $db->list_db($db,$sql);
      $id = $id[0]["id"];
      $sql = "CALL login_reg($id,'$arrArgument[token]');";
      return $db->execute($sql);
    }
    public function vallog_login_DAO($db, $arrArgument) {
      $sql = "SELECT id,user_type FROM users_info WHERE name = '$arrArgument[id]' UNION SELECT id_user,type FROM users_info_FB WHERE name = '$arrArgument[id]'";
      $user = $db->list_db($db,$sql);
      $id = $user[0]["id"];
      $type = $user[0]["user_type"];
      $sql = "SELECT * FROM reg_login WHERE id_user = '$id' AND token = '$arrArgument[token]'";
      $rows = $db->execute($sql)->num_rows;
      $back = array("id" => $id ,"rows" => $rows,"type" => $type); 
      return $back;
    }
    public function read_user_DAO($db,$arrArgument) {
      $token = $arrArgument['token'];
      $sql = "SELECT id FROM users_info WHERE name = '$arrArgument[name]' UNION SELECT id_user FROM users_info_FB WHERE name = '$arrArgument[name]'";
      $user = $db->list_db($db,$sql);
      $id = $user[0]["id"];
      $sql = "CALL read_user('$id','$token')";

      $user = $db->list_db($db,$sql);
      if ($user) {
        $back = array("exists"=>true,"user"=>$user[0]);
      }else
        $back = array("exists"=>false);
      return $back;
    }
}
