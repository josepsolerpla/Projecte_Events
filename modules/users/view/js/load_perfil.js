$(document).ready(function () {
	var user = read_cookie("user");
	document.getElementById("name").innerHTML = user.username;
	var data = {'name':user.username,"token":user.token};
	petition({mod:"users",fun:"read_user",data:data}).then(function (resp){
		resp = JSON.parse(resp);
		if (resp.op == "enter") {
			console.log(resp);
			var user = resp.user;
			document.getElementById("name").innerHTML = user.name;
			document.getElementById("surname").innerHTML = user.surname;
			document.getElementById("email").innerHTML = user.email;
			document.getElementById("photo").src = user.photo;
		};
	});
});